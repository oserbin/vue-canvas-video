const Home = {
  methods: {
    join: function () {
      router.push('main');
    }
  },
  template: `<div class="join"><button class="btn btn-success" v-on:click="join">Join</button></div>`
}

const Main = {
  data: function () {
    return {
      video: null,
      canvas: null,
      context: null,
      stream: null,
      constraints: {},
      cameraState: false,
      comments: null
    }
  },
  created: function() {
    this.fetchComments();
  },
  methods: {
    fetchComments: function() {
      fetch("https://jsonplaceholder.typicode.com/comments")
      .then(response => response.json())
      .then(data => (this.comments = data));
    },
    stop: function() {
      this.video.pause();
      if (this.stream) {
        this.stream.getTracks().forEach(track => {
          track.stop();
        });
        this.video.srcObject = null;
      }
      this.video.removeAttribute("src");
      this.video.load();
      this.cameraState = false;
    },
    start: function() {
      this.stop();
      this.getMedia().then(result => {
        if (result) {
          requestAnimationFrame(this.renderFrame);
          this.cameraState = true;
        }
      });
    },
    renderFrame: function() {
          requestAnimationFrame(this.renderFrame);
          if (this.video && this.video.readyState === this.video.HAVE_ENOUGH_DATA) {
              this.context.drawImage(this.video, 0, 0, this.canvas.width, this.canvas.height);
          }
    },
    getMedia: async function() {
        try {
            if (navigator.mediaDevices) {
              this.setConstraints();
              this.stream = await navigator.mediaDevices.getUserMedia(
                this.constraints
              );
              this.video.srcObject = this.stream;

              return true;
            } else {
              return false;
            }
        } catch (err) {
          throw err;
        }
      },
      setConstraints: function() {
        this.constraints = {
          video: { facingMode: 'environment' },
          audio: false
        };
      },
    },
    mounted: function () {
      this.canvas = document.querySelector("canvas");
      this.video = document.querySelector("video");
      this.context = this.canvas.getContext('2d');
      this.getMedia().then(result => {
        if (result) {
          requestAnimationFrame(this.renderFrame);
          this.cameraState = true;
        }
      });
    },
  template: `<div>
                <div class="actions">
                  <button class="btn btn-primary" v-if="!cameraState" v-on:click="start">Camera</button>
                  <button class="btn btn-secondary" v-if="cameraState" v-on:click="stop">Stop</button>
                </div>
                <div class="video">
                  <video id="video" playsinline autoplay></video>
                  <canvas id="canvas" v-show="cameraState"></canvas>
                </div>
                <div class="comments">
                  <h2>Comments</h2>
                  <ul>
                    <li v-for="comment in comments">
                      <span class="comment-name">{{ comment.name }}</span>
                      <p class="comment-body">{{ comment.body }}</p>
                      </li>
                  </ul>
                </div>
            </div>`
}

const routes = [
  { path: '/', component: Home },
  { path: '/main', component: Main }
]

const router = new VueRouter({
  routes
})

const app = new Vue({
  router
}).$mount('#app')
